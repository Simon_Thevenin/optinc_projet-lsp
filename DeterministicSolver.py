from pulp import *
import numpy as np
from Solution import Solution


class DeterministicSolver(object):
    def __init__(self,instance):
        self.Model = LpProblem("DeterministicProductionPlan",LpMinimize)
        self.Instance = instance
        #Generate the deterministic demand through the normal distribution
        self.DeterminiticDelta = None
        self.GenerateDeterministicDelta()

    def CreateModel(self):
        self.CreateVariables()
        self.CreateObjFunction()
        self.CreateConstraints()

    def CreateVariables(self):
        # class pulp.LpVariable(name, lowBound=None, upBound=None, cat='Continuous', e=None)
        #Production quantities
        self.PdQtd = pulp.LpVariable.dicts('PdQtd',
                                         ((i,t) for i in self.Instance.SetItems
                                          for t in self.Instance.SetTimePeriod),
                                         lowBound=0,
                                         cat=pulp.LpContinuous)
        #inventory level
        self.InvLvl = pulp.LpVariable.dicts('InvLvl',
                                           ((i, t) for i in self.Instance.SetItems
                                            for t in self.Instance.SetTimePeriod),
                                           lowBound=0,
                                           cat=pulp.LpContinuous)
        #backlob order
        self.Backlog = pulp.LpVariable.dicts('Backlog',
                                            ((i, t) for i in self.Instance.SetItems
                                             for t in self.Instance.SetTimePeriod),
                                            lowBound=0,
                                            cat=pulp.LpContinuous)
        #setup
        self.Setup = pulp.LpVariable.dicts('Setup',
                                             ((i, t) for i in self.Instance.SetItems
                                              for t in self.Instance.SetTimePeriod),
                                             lowBound=0,
                                             cat=pulp.LpBinary)


    def CreateObjFunction(self):
        self.Model += (
            pulp.lpSum([
                        self.Instance.SetupCost[i] * self.Setup[(i,t)] + self.Instance.HoldingCost[i] * self.InvLvl[(i,t)] + self.Instance.BackloggingCost[i] * self.Backlog[(i,t)]
                        for i in self.Instance.SetItems for t in self.Instance.SetTimePeriod
                       ])
            )

    def CreateConstraints(self):
        linkVariable = None
        capa = None

        ## Inventory Balance
        #as Bi,0 = 0 and Ii,0 is a given data then the balance constraint for t = 0 is X(i,t) + I(i,0) - (I(i,t)-B(i,t) = d(it)
        for i in self.Instance.SetItems:
            self.Model += self.PdQtd[(i,0)] + self.Instance.InitialInventory[i] - (self.InvLvl[(i,0)] - self.Backlog[(i,0)]) == self.DeterminiticDelta[i][0]
        # remaining cases
        for t in self.Instance.SetTimePeriod[1:]:
            for i in self.Instance.SetItems:
                self.Model += self.PdQtd[(i,t)] + (self.InvLvl[(i,t-1)] - self.Backlog[(i,t-1)]) - (self.InvLvl[(i,t)] - self.Backlog[(i,t)]) == self.DeterminiticDelta[i][t]

        # Capacity constraint
        for t in self.Instance.SetTimePeriod:
            #capacity constraint for period
            self.Model += sum(self.PdQtd[(i,t)] for i in self.Instance.SetItems) <= self.Instance.Capacity
            for i in self.Instance.SetItems:
                # link continuous and binary variables
                self.Model += self.PdQtd[(i,t)] <= self.Instance.Capacity * self.Setup[(i,t)]

    def WriteLP(self):
        self.Model.writeLP("DeterministicModel.lp")

    def SolveLP(self):
        self.Model.solve(CPLEX(msg=1))

    def GetSolution(self):
        sol = Solution()
        sol.totalCost = value(self.Model.objective)
        sol.setup = [[self.Setup[(i,t)].varValue
                              for i in self.Instance.SetItems]
                             for t in self.Instance.SetTimePeriod]
        sol.production = [[self.PdQtd[(i, t)].varValue
                      for i in self.Instance.SetItems]
                     for t in self.Instance.SetTimePeriod]
        sol.inventory = [[self.InvLvl[(i, t)].varValue
                           for i in self.Instance.SetItems]
                          for t in self.Instance.SetTimePeriod]
        sol.backlog = [[self.Backlog[(i, t)].varValue
                          for i in self.Instance.SetItems]
                         for t in self.Instance.SetTimePeriod]
        return sol

    def GenerateDeterministicDelta(self):
        #self.DeterminiticDelta[i][t] is a range from avg-stdDev to avg+stdDev over all the planning horizon
        self.DeterminiticDelta = [0] * self.Instance.NbItems
        for i in self.Instance.SetItems:
            self.DeterminiticDelta[i] = np.around(list(np.linspace(self.Instance.AvgDemand[i],self.Instance.StdDeviationDemand[i],self.Instance.NbPeriods)))
        #print(self.DeterminiticDelta)


