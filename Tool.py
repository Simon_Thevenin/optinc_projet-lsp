
import itertools as itools
import pandas as pd

class Tool:

# This function transform the sheet given in arguments into a dataframe
    @staticmethod
    def ReadDataFrame(wb2, framename):
        sheet = wb2[framename];
        data = sheet.values
        cols = next(data)[1:]
        cols = list(cols)
        # remove the None from the column names
        for i in range(len(cols)):
            if cols[i] == None:
                cols[i] = i

        data = list(data)
        idx = [r[0] for r in data]
        data = (itools.islice(r, 1, None) for r in data)
        df = pd.DataFrame(data, index=idx, columns=cols)
        return df;
