from pulp import *
from pyDOE import *
from scipy.stats import norm
import numpy as np
from Solution import Solution

class StochasticSolver( object ):
    def __init__(self, instance):
        self.Model = LpProblem("DeterministicProductionPlan", LpMinimize)
        self.Instance = instance
        self.SetScenario = [i for i in range(200)]
        self.ProbaScenario = []
        self.StochasticDelta = None
        self.SampleStrategy = "CMC"
        # CMC == critical monte carlo // SMC == sequential monte carlo LHS == Latin Hypercube Sampling
        self.simulation = False
        #self.BigM = sum(self.Instance.Demand[t] for t in self.Instance.SetTimePeriod)

    def CreateModel(self, n):
        self.GenerateScenario(n)
        self.CreateVariables()
        self.CreateObjFunction()
        self.CreateConstraints()

    def CreateVariables(self):
        # Production quantities
        self.PdQtd = pulp.LpVariable.dicts('PdQtd',
                                           ((i, t) for i in self.Instance.SetItems
                                            for t in self.Instance.SetTimePeriod),
                                           lowBound=0,
                                           cat=pulp.LpContinuous)
        # inventory level
        self.Iitw = pulp.LpVariable.dicts('InvLvl',
                                            ((i, t, w) for i in self.Instance.SetItems
                                             for t in self.Instance.SetTimePeriod
                                             for w in self.SetScenario),
                                            lowBound=0,
                                            cat=pulp.LpContinuous)
        # backlob order
        self.Bitw = pulp.LpVariable.dicts('Backlog',
                                             ((i, t, w) for i in self.Instance.SetItems
                                              for t in self.Instance.SetTimePeriod
                                              for w in self.SetScenario),
                                             lowBound=0,
                                             cat=pulp.LpContinuous)
        # setup
        self.Setup = pulp.LpVariable.dicts('Setup',
                                           ((i, t) for i in self.Instance.SetItems
                                            for t in self.Instance.SetTimePeriod),
                                           lowBound=0,
                                           cat=pulp.LpBinary)

    def CreateObjFunction(self):
        self.Model += (
            pulp.lpSum([
                        self.ProbaScenario[w] * (self.Instance.SetupCost[i] * self.Setup[(i,t)] + self.Instance.HoldingCost[i] * self.Iitw[(i,t,w)] + self.Instance.BackloggingCost[i] * self.Bitw[(i,t,w)])
                        for i in self.Instance.SetItems for t in self.Instance.SetTimePeriod for w in self.SetScenario
                       ])
            )

    def CreateConstraints(self):
        linkVariable = None
        capa = None

        ## Inventory Balance
        #as Bi,0 = 0 and Ii,0 is a given data then the balance constraint for t = 0 is X(i,t) + I(i,0,w) - (I(i,t,w)-B(i,t,w) = d(it)
        for i in self.Instance.SetItems:
            for w in self.SetScenario:
                self.Model += self.PdQtd[(i,0)] + self.Instance.InitialInventory[i] - (self.Iitw[(i,0,w)] - self.Bitw[(i,0,w)]) == self.StochasticDelta[w][i][0]
        # remaining cases
        for t in self.Instance.SetTimePeriod[1:]:
            for i in self.Instance.SetItems:
                for w in self.SetScenario:
                    self.Model += self.PdQtd[(i,t)] + (self.Iitw[(i,t-1,w)] - self.Bitw[(i,t-1,w)]) - (self.Iitw[(i,t,w)] - self.Bitw[(i,t,w)]) == self.StochasticDelta[w][i][t]

        # Capacity constraint
        for t in self.Instance.SetTimePeriod:
            #capacity constraint for period
            self.Model += sum(self.PdQtd[(i,t)] for i in self.Instance.SetItems) <= self.Instance.Capacity
            for i in self.Instance.SetItems:
                # link continuous and binary variables
                self.Model += self.PdQtd[(i,t)] <= self.Instance.Capacity * self.Setup[(i,t)]

    def WriteLP(self):
        self.Model.writeLP("StochasticModel.lp")

    def SolveLP(self):
        self.Model.solve(CPLEX(msg=0))
        print("Status:", LpStatus[self.Model.status])

    def GetSolution(self):
        if  not LpStatus[self.Model.status] == "Infeasible":
            sol = Solution()
            sol.totalCost = value(self.Model.objective)
            sol.setup = [[self.Setup[(i, t)].varValue
                          for i in self.Instance.SetItems]
                         for t in self.Instance.SetTimePeriod]
            sol.production = [[self.PdQtd[(i, t)].varValue
                               for i in self.Instance.SetItems]
                              for t in self.Instance.SetTimePeriod]
            sol.inventory = [[[self.Iitw[(i, t, w)].varValue
                              for i in self.Instance.SetItems]
                              for t in self.Instance.SetTimePeriod]
                              for w in self.SetScenario]
            sol.backlog = [[[self.Bitw[(i, t, w)].varValue
                            for i in self.Instance.SetItems]
                            for t in self.Instance.SetTimePeriod]
                            for w in self.SetScenario]

            return sol
        else:
            return None

    def GenerateScenario(self, n):
        self.SetScenario = [ w for w in range(n)]
        self.ProbaScenario=[1.0 / n for w in range(n)]


        L01 = None
        if (self.SampleStrategy == "CMC"):
                    L01 = [[[np.random.uniform(0, 1)
                                   for t in self.Instance.SetTimePeriod]
                                  for i in self.Instance.SetItems]
                                 for w in self.SetScenario]
        if(self.SampleStrategy == "LHS"):
             Lv01 = lhs(self.Instance.NbPeriods * self.Instance.NbItems, samples=n)
             L01 =  L01 = [[[Lv01[w][i*len(self.Instance.SetTimePeriod)+t]
                     for t in self.Instance.SetTimePeriod]
                     for i in self.Instance.SetItems]
                     for w in self.SetScenario]

        self.StochasticDelta = [[[0
             for t in self.Instance.SetTimePeriod]
             for i in self.Instance.SetItems]
             for w in self.SetScenario]
        for w in range(n):
                for t in self.Instance.SetTimePeriod:
                    for i in self.Instance.SetItems:
                         self.StochasticDelta[w][i][t] = norm(loc=self.Instance.AvgDemand[i], scale=self.Instance.StdDeviationDemand[i]).ppf(L01[w][i][t])



    def Simulator(self, PlannedProd, PlannedSetUp):
        self.simulation = True
        self.SampleStrategy = "CMC"
        self.GenerateScenario(1000)
        self.CreateVariables()
        self.CreateObjFunction()
        self.CreateConstraints()

        for t in self.Instance.SetTimePeriod:
            for i in self.Instance.SetItems:
                self.Model += self.PdQtd[(i,t)] == PlannedProd[t][i]
                self.Model += self.Setup[(i, t)] == PlannedSetUp[t][i]
