from typing import List, Any


class Instance(object):
    def __index__(self):
        self.NbPeriods = -1
        self.NbItems = -1
        self.Capacity = -1
        self.SetItems = []
        self.SetupCost = []
        self.HoldingCost = []
        self.BackloggingCost = []
        self.AvgDemand = []
        self.StdDeviationDemand = []
        self.InitialInventory = []

    def Print(self):
        print("nb period: %r" % self.NbPeriods)
        print("nb items: %r" % self.NbItems)

        print("set time periods: %r" % self.SetTimePeriod)
        print("set items: %r" % self.SetItems)

        print("capacity: %r" % self.Capacity)
        print("setup costs: %r" % self.SetupCost)
        print("holding costs: %r" % self.HoldingCost)
        print("backlogging costs: %r" % self.BackloggingCost)
        print("initial inventory %r" % self.InitialInventory)
        print("average demand %r" % self.AvgDemand)
        print("standard deviation demand %r" % self.StdDeviationDemand)
