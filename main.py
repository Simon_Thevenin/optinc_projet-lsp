from Instance import Instance
from InstanceReader import InstanceReader
from DeterministicSolver import DeterministicSolver
from StochasticSolver import StochasticSolver


def Simulate(qt1,qt2):
    simulator = StochasticSolver(instance)
    simulator.Simulator(qt1,qt2)
    simulator.SolveLP()
    simulatedsolution = simulator.GetSolution()
    if not simulatedsolution is None:
        simulatedsolution.Print()

if __name__ == '__main__':
    print("------------Hello----------------")


    reader = InstanceReader("./Data/DATA_Prod.xlsx")
    reader.Read()
    instance = reader.Instance
    instance.Print()

    solver = DeterministicSolver(instance)
    solver.CreateModel()
    #solver.WriteLP()
    solver.SolveLP()

    solution = solver.GetSolution()
    solution.Print()

    Simulate(solution.production, solution.setup)

    for sampling in ["CMC", "LHS"]:
        for n in [2, 5, 10, 50, 100]:
            print("*****************" )
            print("n %r sample:%r" %(n, sampling))
            print("****OPT****")
            solver = StochasticSolver(instance)
            solver.SampleStrategy = sampling
            solver.CreateModel(n)
            #solver.WriteLP()
            solver.SolveLP()

            solution = solver.GetSolution()
            solution.Print()
            print("****SIM****")
            Simulate(solution.production,solution.setup)
            # print("****Simulation****")
            # wait = input("PRESS ENTER TO CONTINUE.")


