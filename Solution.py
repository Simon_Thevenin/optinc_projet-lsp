from Instance import  Instance
import openpyxl as opxl
import pandas as pd
from Tool import Tool

class Solution:
    # constructor
    def __init__(self, instance=None, setup = None, production=None, inventory=None, backlog=None):
        self.instance = instance
        self.setup = setup
        self.production = production
        self.inventory = inventory
        self.backlog = backlog
        self.totalCost = -1

    def Print(self):
        print("Total cost %r" % self.totalCost)
       # print("Setup decision %r" % self.setup)
       # print("Production quantities %r" % self.production)
       # print("Inventory level %r" % self.inventory)
       # print("Backlog order %r" % self.backlog)

    # This function print the solution different pickle files
    # This function print the solution in an Excel file in the folde "Solutions"
    def PrintToExcel(self, description):
        prodquantitydf, inventorydf, productiondf, backlogdf, svaluedf, fixedqvaluesdf = self.DataFrameFromList()
        writer = pd.ExcelWriter(self.GetSolutionFileName(description), engine='openpyxl')
        prodquantitydf.to_excel(writer, 'ProductionQuantity')
        inventorydf.to_excel(writer, 'InventoryLevel')
        backlogdf.to_excel(writer, 'Backlog')

        writer.save()


    def ReadExcelFiles(self, description, index = "", indexbackorder = ""):
        # The supplychain is defined in the sheet named "01_LL" and the data are in the sheet "01_SD"
        prodquantitydf = Tool.ReadMultiIndexDataFrame(self.GetSolutionFileName(description), "ProductionQuantity" )
        return prodquantitydf
