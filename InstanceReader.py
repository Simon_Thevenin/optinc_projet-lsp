from Instance import  Instance
import openpyxl as opxl
import itertools as itools
import pandas as pd
from Tool import Tool

class InstanceReader():
        # Constructor
        def __init__(self, filename=""):
            self.Instance = Instance()
            self.Instance.NbPeriods = 10
            self.Instance.SetTimePeriod = [i for i in range(self.Instance.NbPeriods)]
            self.Data = None
            self.ConvData = []
            self.Filename = filename
            self.ParameterName = []
            self.NbParameter = -1
            # This funciton read the instance from a .xlsx unique sheet file

        def Read(self):
            self.OpenFiles(self.Filename)
            self.CreateData()


        # Create dataset from the sheet for instance from Data
        def OpenFiles(self, instancename):
            wb2 = opxl.load_workbook(self.Filename)
            self.Data = Tool.ReadDataFrame(wb2, "data")

            #Converting panda dataframe to array
            self.ConvData = self.Data.to_numpy();

        def CreateData(self):
            self.Instance.ItemName = []
            self.Parameters = []



        #Getting table headings
            for row in self.Data.index:
                self.Instance.ItemName.append(row)

            for column in self.Data.columns:
                self.Parameters.append(column)

            self.Instance.NbItems = len(self.Instance.ItemName) -1
            self.Instance.SetItems = [i for i in range(self.Instance.NbItems)]
            #self.NbParameter = len(self.Parameters)
            #self.ParameterName = [i for i in range(self.NbParameter)]



            # Iniciating parameters
            self.Instance.InitialInventory = [0] * self.Instance.NbItems
            self.Instance.SetupCost = [0] * self.Instance.NbItems
            self.Instance.HoldingCost = [0] * self.Instance.NbItems
            self.Instance.BackloggingCost = [0] * self.Instance.NbItems
            self.Instance.AvgDemand = [0] * self.Instance.NbItems
            self.Instance.StdDeviationDemand = [0] * self.Instance.NbItems

            #Reading parameters values for each item
            for p in self.Instance.SetItems:
                self.Instance.SetupCost[p] = self.ConvData[p, 0]
                self.Instance.InitialInventory[p] = self.ConvData[p,1]
                self.Instance.HoldingCost[p] = self.ConvData[p,2]
                self.Instance.BackloggingCost[p] = self.ConvData[p,3]
                self.Instance.AvgDemand[p] = self.ConvData[p,4]
                self.Instance.StdDeviationDemand[p] = self.ConvData[p,5]

            # reading capacity
            self.Instance.Capacity = self.ConvData[len(self.Instance.ItemName)-1,0]

